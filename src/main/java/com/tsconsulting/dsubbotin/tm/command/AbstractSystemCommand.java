package com.tsconsulting.dsubbotin.tm.command;

import org.jetbrains.annotations.Nullable;

public abstract class AbstractSystemCommand extends AbstractCommand {

    public abstract String arg();

    @Override
    public String toString() {
        String result = super.toString();
        @Nullable final String arg = arg();
        if (arg != null && !arg.isEmpty()) result += " (" + arg + ")";
        return result;
    }

}
