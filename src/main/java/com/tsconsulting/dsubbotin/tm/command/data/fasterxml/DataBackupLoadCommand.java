package com.tsconsulting.dsubbotin.tm.command.data.fasterxml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.tsconsulting.dsubbotin.tm.command.data.AbstractFasterXmlDataCommand;
import com.tsconsulting.dsubbotin.tm.dto.Domain;
import org.jetbrains.annotations.NotNull;

import java.io.File;

public class DataBackupLoadCommand extends AbstractFasterXmlDataCommand {

    @Override
    public @NotNull String name() {
        return "backup-load";
    }

    @Override
    public @NotNull String description() {
        return "Load backup data.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ObjectMapper objectMapper = getObjectMapper(new XmlMapper());
        @NotNull final File file = new File(BACKUP_XML);
        if (!file.canRead()) return;
        @NotNull final Domain domain = objectMapper.readValue(file, Domain.class);
        setDomain(domain);
    }

}
