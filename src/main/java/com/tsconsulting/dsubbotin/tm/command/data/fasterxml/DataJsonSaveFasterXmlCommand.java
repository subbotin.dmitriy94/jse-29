package com.tsconsulting.dsubbotin.tm.command.data.fasterxml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.tsconsulting.dsubbotin.tm.command.data.AbstractFasterXmlDataCommand;
import com.tsconsulting.dsubbotin.tm.dto.Domain;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

import java.io.File;

public final class DataJsonSaveFasterXmlCommand extends AbstractFasterXmlDataCommand {

    @Override
    public @NotNull String name() {
        return "data-save-json-fasterxml";
    }

    @Override
    public @NotNull String description() {
        return "Data saved in JSON using fasterxml.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ObjectMapper objectMapper = getObjectMapper(new JsonMapper());
        @NotNull final File file = new File(FILE_JSON_FASTERXML);
        @NotNull final Domain domain = getDomain();
        objectMapper.writeValue(file, domain);
        TerminalUtil.printMessage("Save to JSON completed.");
    }

}
