package com.tsconsulting.dsubbotin.tm.command.data.fasterxml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.tsconsulting.dsubbotin.tm.command.data.AbstractFasterXmlDataCommand;
import com.tsconsulting.dsubbotin.tm.dto.Domain;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

import java.io.File;

public final class DataXmlLoadFasterXmlCommand extends AbstractFasterXmlDataCommand {

    @Override
    public @NotNull String name() {
        return "data-load-xml-fasterxml";
    }

    @Override
    public @NotNull String description() {
        return "Data loaded from XML using fasterxml.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ObjectMapper objectMapper = getObjectMapper(new XmlMapper());
        @NotNull final File file = new File(FILE_XML_FASTERXML);
        @NotNull final Domain domain = objectMapper.readValue(file, Domain.class);
        setDomain(domain);
        TerminalUtil.printMessage("Load completed.");
        TerminalUtil.printMessage("Logged out. Please log in.");
    }

}
