package com.tsconsulting.dsubbotin.tm.command.data.jaxb;

import com.tsconsulting.dsubbotin.tm.command.data.AbstractJaxbDataCommand;
import com.tsconsulting.dsubbotin.tm.dto.Domain;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.Unmarshaller;
import java.io.File;

public final class DataXmlLoadJaxbCommand extends AbstractJaxbDataCommand {

    @Override
    public @NotNull String name() {
        return "data-load-xml-jaxb";
    }

    @Override
    public @NotNull String description() {
        return "Data loaded from XML using JAXB.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Unmarshaller jaxbUnmarshaller = getJaxbUnmarshaller();
        @NotNull final File file = new File(FILE_XML_JAXB);
        @NotNull final Domain domain = (Domain) jaxbUnmarshaller.unmarshal(file);
        setDomain(domain);
        TerminalUtil.printMessage("Load completed.");
        TerminalUtil.printMessage("Logged out. Please log in.");
    }

}
