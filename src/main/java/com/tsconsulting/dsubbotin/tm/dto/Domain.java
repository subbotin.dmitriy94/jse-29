package com.tsconsulting.dsubbotin.tm.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.model.Task;
import com.tsconsulting.dsubbotin.tm.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonRootName(value = "domain")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Domain implements Serializable {

    @NotNull
    @XmlElement(name = "user")
    @XmlElementWrapper(name = "users")
    private List<User> users;

    @NotNull
    @XmlElement(name = "project")
    @XmlElementWrapper(name = "projects")
    private List<Project> projects;

    @NotNull
    @XmlElement(name = "task")
    @XmlElementWrapper(name = "tasks")
    private List<Task> tasks;

}
