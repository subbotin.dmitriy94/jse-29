package com.tsconsulting.dsubbotin.tm.model;

import com.tsconsulting.dsubbotin.tm.api.entity.IWBS;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.util.DateAdapter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@XmlRootElement
public final class Task extends AbstractOwnerEntity implements IWBS {

    @NotNull
    private String name;

    @NotNull
    private String description;

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    private String projectId = null;

    @Nullable
    @XmlElement
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date startDate;

    @NotNull
    @XmlElement
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date createDate = new Date();

    public Task(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    @NotNull
    public String toString() {
        return super.toString() + name + "; " +
                "Project id: " + projectId + "; " +
                "Status: " + status + "; " +
                "Started: " + startDate + "; " +
                "Created: " + createDate + ";";
    }

}