package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import org.jetbrains.annotations.NotNull;

import java.util.Date;
import java.util.Optional;

public final class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    @Override
    @NotNull
    public Project findByName(@NotNull final String userId, @NotNull final String name) throws AbstractException {
        return findAll(userId).stream()
                .filter(p -> p.getName().equals(name))
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public void removeByName(@NotNull final String userId, @NotNull final String name) throws AbstractException {
        @NotNull final Optional<Project> optional = Optional.of(findByName(userId, name));
        optional.ifPresent(this::remove);
    }

    @Override
    public void updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        @NotNull final Optional<Project> optional = Optional.of(findById(userId, id));
        optional.ifPresent(p -> {
            p.setName(name);
            p.setDescription(description);
        });
    }

    @Override
    public void updateByIndex(
            @NotNull final String userId,
            final int index,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        @NotNull final Project project = findByIndex(userId, index);
        project.setName(name);
        project.setDescription(description);
    }

    @Override
    public void startById(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        @NotNull final Optional<Project> optional = Optional.of(findById(userId, id));
        optional.ifPresent(p -> {
            p.setStatus(Status.IN_PROGRESS);
            p.setStartDate(new Date());
        });
    }

    @Override
    public void startByIndex(@NotNull final String userId, final int index) throws AbstractException {
        @NotNull final Project project = findByIndex(userId, index);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
    }

    @Override
    public void startByName(@NotNull final String userId, @NotNull final String name) throws AbstractException {
        @NotNull final Optional<Project> optional = Optional.of(findByName(userId, name));
        optional.ifPresent(p -> {
            p.setStatus(Status.IN_PROGRESS);
            p.setStartDate(new Date());
        });
    }

    @Override
    public void finishById(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        @NotNull final Optional<Project> optional = Optional.of(findById(userId, id));
        optional.ifPresent(p -> p.setStatus(Status.COMPLETED));
    }

    @Override
    public void finishByIndex(@NotNull final String userId, final int index) throws AbstractException {
        @NotNull final Project project = findByIndex(userId, index);
        project.setStatus(Status.COMPLETED);
    }

    @Override
    public void finishByName(@NotNull final String userId, @NotNull final String name) throws AbstractException {
        @NotNull final Optional<Project> optional = Optional.of(findByName(userId, name));
        optional.ifPresent(p -> p.setStatus(Status.COMPLETED));
    }

    @Override
    public void updateStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    ) throws AbstractException {
        @NotNull final Optional<Project> optional = Optional.of(findById(userId, id));
        optional.ifPresent(p -> {
            p.setStatus(status);
            if (status == Status.IN_PROGRESS) p.setStartDate(new Date());
        });
    }

    @Override
    public void updateStatusByIndex(
            @NotNull final String userId,
            final int index,
            @NotNull final Status status
    ) throws AbstractException {
        @NotNull final Project project = findByIndex(userId, index);
        project.setStatus(status);
        if (status == Status.IN_PROGRESS) project.setStartDate(new Date());
    }

    @Override
    public void updateStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final Status status
    ) throws AbstractException {
        @NotNull final Optional<Project> optional = Optional.of(findByName(userId, name));
        optional.ifPresent(p -> {
            p.setStatus(status);
            if (status == Status.IN_PROGRESS) p.setStartDate(new Date());
        });
    }

}