package com.tsconsulting.dsubbotin.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.Nullable;

@UtilityClass
public final class EmptyUtil {

    public static boolean isEmpty(@Nullable final String value) {
        return value == null || value.isEmpty();
    }

}
